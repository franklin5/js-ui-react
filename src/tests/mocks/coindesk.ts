export const coinDeskData  = {
    "time": {
        "updated": "Sep 2, 2023 17:47:00 UTC",
        "updatedISO": "2023-09-02T17:47:00+00:00",
        "updateduk": "Sep 2, 2023 at 18:47 BST"
    },
    "disclaimer": "This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
    "chartName": "Bitcoin",
    "bpi": {
        "USD": {
            "code": "USD",
            "symbol": "&#36;",
            "rate": "25,837.7329",
            "description": "United States Dollar",
            "rate_float": 25837.7329
        },
        "GBP": {
            "code": "GBP",
            "symbol": "&pound;",
            "rate": "21,589.8029",
            "description": "British Pound Sterling",
            "rate_float": 21589.8029
        },
        "EUR": {
            "code": "EUR",
            "symbol": "&euro;",
            "rate": "25,169.7242",
            "description": "Euro",
            "rate_float": 25169.7242
        }
    }
}