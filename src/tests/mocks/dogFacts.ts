export const dogFacts = {
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "fact": "This is dog fact number 1",
            "created_at": "2023-09-01T09:09:38.000000Z",
            "updated_at": "2023-09-01T09:09:38.000000Z"
        },
        {
            "id": 2,
            "fact": "This is dog fact number 2",
            "created_at": "2023-09-01T09:10:38.000000Z",
            "updated_at": "2023-09-01T09:10:38.000000Z"
        }
    ],
    "per_page": 10,
    "total": 10
}