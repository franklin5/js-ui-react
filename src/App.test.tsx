import React from 'react';
import {render, screen} from '@testing-library/react';
import OrderList from './components/OrderList';
import Deposits from './Deposits';
import {coinDeskData} from "./tests/mocks/coindesk";
import Bitcoin from "./Bitcoin";
import {MemoryRouter} from "react-router-dom";
import {formatAmount} from "./helper";
import DogFacts from "./components/DogFacts";
import {dogFacts} from "./tests/mocks/dogFacts";

test('renders orders', () => {
  render(<OrderList />);
  const title = screen.getByText(/recent money/i);
  expect(title).toBeInTheDocument();
});

test('renders deposits', () => {
  render(<Deposits />);
  const title = screen.getByText(/Recent Deposits/i);
  expect(title).toBeInTheDocument();
});

test('bitcoin page', async () => {
  global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve(coinDeskData),
      }),
  ) as jest.Mock;

  render( <MemoryRouter><Bitcoin /></MemoryRouter>);
  expect(await screen.findByText(coinDeskData.disclaimer)).toBeInTheDocument()
  expect(await screen.findByText(coinDeskData.bpi.USD.code)).toBeInTheDocument()
  expect(await screen.findByText(coinDeskData.bpi.USD.description)).toBeInTheDocument()
  expect(await screen.findByText(formatAmount(coinDeskData.bpi.USD.rate_float))).toBeInTheDocument()

  expect(await screen.findByText(coinDeskData.bpi.EUR.code)).toBeInTheDocument()
  expect(await screen.findByText(coinDeskData.bpi.EUR.description)).toBeInTheDocument()
  expect(await screen.findByText(formatAmount(coinDeskData.bpi.EUR.rate_float))).toBeInTheDocument()

  expect(await screen.findByText(coinDeskData.bpi.GBP.code)).toBeInTheDocument()
  expect(await screen.findByText(coinDeskData.bpi.GBP.description)).toBeInTheDocument()
  expect(await screen.findByText(formatAmount(coinDeskData.bpi.GBP.rate_float))).toBeInTheDocument()

  expect(await screen.findByText('Bitcoin Currency Exchange')).toBeInTheDocument()
});

test('dog fact', async () => {
  global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve(dogFacts),
      }),
  ) as jest.Mock;

  render( <DogFacts />);
  expect(await screen.findByText(dogFacts.data[0].fact)).toBeInTheDocument()
  expect(await screen.findByText(dogFacts.data[1].fact)).toBeInTheDocument()
  expect(await screen.findByText(dogFacts.total)).toBeInTheDocument()
});
