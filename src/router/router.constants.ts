export const DASHBOARD_ROUTE = '/'
export const BITCOIN_ROUTE = '/bitcoin'
export const DOG_FACTS_ROUTE = '/dog-facts'
export const ORDER_ROUTE = '/orders'