import {createBrowserRouter} from 'react-router-dom';
import {BITCOIN_ROUTE, DASHBOARD_ROUTE, DOG_FACTS_ROUTE, ORDER_ROUTE} from "./router.constants";
import Dashboard from "../Dashboard";
import Bitcoin from "../Bitcoin";
import Orders from "../Orders";
import DogFacts from "../components/DogFacts";

export const router  = createBrowserRouter([
    {
        path: DASHBOARD_ROUTE,
        element: <Dashboard />
    },
    {
        path: BITCOIN_ROUTE,
        element: <Bitcoin />
    },
    {
        path: ORDER_ROUTE,
        element: <Orders />
    },
    {
        path: DOG_FACTS_ROUTE,
        element: <DogFacts />
    },
])