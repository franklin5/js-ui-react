import * as React from 'react';
import {useEffect, useState} from 'react';
import Typography from '@mui/material/Typography';
import {DashboardLayout} from "./Layout/DashboardLayout";
import Grid from "@mui/material/Grid";
import {Card, CardContent, CircularProgress} from "@mui/material";
import Box from "@mui/material/Box";
import {formatAmount, formatDate} from "./helper";
import {styled} from "@mui/material/styles";
import {getBitcoinPrice} from "./services";

const Preloader = styled(Box)({
    display: 'flex', justifyContent:'center', marginBottom: '12px'
})

export default function Bitcoin() {
    const [apiData, setApiData]: [any, any] = useState();
    const [loading, setLoading]: [any, any] = useState(true);

    useEffect(()=>{
        getBitcoinPrice((data)=>{
            setApiData(data)
            setLoading(false)
        });
    }, []);
  return (
    <DashboardLayout title="Bitcoin Currency Exchange">
        {loading && <Preloader component="div"><CircularProgress /></Preloader>}
        <Grid container spacing={3}>
            {
                apiData && Object.entries(apiData?.bpi).map(([key, val]:[any, any], index)=>(
                    <Grid item xs={12} md={4} lg={4} key={index}>
                        <Card>
                            <CardContent>
                                <Typography sx={{ fontSize: 14, fontWeight:'bold' }} color="text.secondary" gutterBottom>
                                    {val.code}
                                </Typography>
                                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                    {val.description}
                                </Typography>
                                <Typography variant="h5" component="div">
                                    {formatAmount(val.rate_float)}
                                </Typography>

                                <Typography variant="body2" sx={{ marginTop: "20px" }}>
                                    Updated at:
                                    <br />
                                    {formatDate(apiData.time.updated)}
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                ))
            }
        </Grid>
        {
            apiData && <Box component="p" sx={{ p: 2, fontSize: "12px" }}>
                {apiData.disclaimer}
            </Box>
        }
    </DashboardLayout>
  );
}