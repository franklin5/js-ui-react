import * as React from 'react';
import {DashboardLayout} from "./Layout/DashboardLayout";
import OrderList from "./components/OrderList";

export default function Orders() {
  return (
    <DashboardLayout title="Orders">
      <OrderList />
    </DashboardLayout>
  );
}