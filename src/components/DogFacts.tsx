import * as React from 'react';
import {useEffect, useState} from 'react';
import {TablePagination} from "@mui/material";
import {formatDate} from "../helper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import Paper from "@mui/material/Paper";
import {getDogFacts} from "../services";
import Title from "../Title";

interface dogFact {
    id: number,
    fact: string,
    created_at: string
}

export default function DogFacts() {
    const [dogFacts, setDogFacts] = useState([]);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [totalPages, setTotalPages] = React.useState(10);

    useEffect(()=>{
        getDogFacts((data)=>{
            setDogFacts(data.data)
            setTotalPages(data.total)
        }, page === 0 ? 1 : page, rowsPerPage)
    }, [page, rowsPerPage]);

    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

  return (
      <React.Fragment>
          <Title>Dog Facts</Title>
          {
              dogFacts && (
                 <>
                     <Table size="small">
                         <TableHead>
                             <TableRow>
                                 <TableCell>Facts</TableCell>
                                 <TableCell>Date</TableCell>
                             </TableRow>
                         </TableHead>
                         <TableBody>
                             {dogFacts.map((row: dogFact) => (
                                 <TableRow key={row.id}>
                                     <TableCell>{row.fact}</TableCell>
                                     <TableCell>{formatDate(row.created_at)}</TableCell>
                                 </TableRow>
                             ))}
                         </TableBody>
                     </Table>
                     <TablePagination
                         component="div"
                         count={totalPages}
                         page={page}
                         onPageChange={handleChangePage}
                         rowsPerPage={rowsPerPage}
                         onRowsPerPageChange={handleChangeRowsPerPage}
                     />
                 </>
              )
          }

      </React.Fragment>
  );
}