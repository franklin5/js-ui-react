import moment from "moment/moment";

export const formatAmount = (amount: number)=>{
    return Number(amount.toFixed(2)).toLocaleString()
}

export const formatDate = (date: string)=>{
    return moment(new Date(date)).format('llll')
}