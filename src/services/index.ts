const fetchRequest = async (url: string) =>{
    const response  = await fetch(url)
    return await response.json()
}

export const getBitcoinPrice = (callback: (data:any)=>void) => {
    fetchRequest('https://api.coindesk.com/v1/bpi/currentprice.json')
        .then(data=>{
            callback(data)
        })
}

export const getDogFacts= (callback: (data:any)=>void, page: number = 1, limit: number = 10) => {
    // @ts-ignore
    const params = new URLSearchParams({
        page,
        pageSize: limit
    })
    fetchRequest('http://localhost:8042/api/v1/dog-facts?' + params)
        .then(data=>{
            callback(data)
        })
}